<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Project
 *
 * @ORM\Table(name="environment")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EnvironmentRepository")
 */
class Environment
{
    const ALLOW_ONLY_OWNER = 1;
    const ALLOW_ONLY_PROJECT = 0;
    const ALLOW_PUBLIC = -1;

    /**
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="environments")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    protected $owner;

    /**
     * @ORM\Column(type="integer")
     */
    protected $privacy;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Fish\Fish")
     * @ORM\JoinColumn(name="fish_id", referencedColumnName="id")
     */
    private $fish;
    
    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Fish\Options", cascade={"persist"})
     * @ORM\JoinColumn(name="options_id", referencedColumnName="id")
     */
    private $options;

    /**
     * @ORM\OneToMany(targetEntity="Result", mappedBy="environment", cascade={"persist"})
     */
    private $results;
    
    /**
     * @ORM\OneToMany(targetEntity="MatlabResult", mappedBy="environment", cascade={"persist"})
     */
    private $matlabResults;

    /**
     * @return mixed
     */
    public function getMatlabResults()
    {
        return $this->matlabResults;
    }

    /**
     * @param mixed $matlabResults
     */
    public function setMatlabResults($matlabResults)
    {
        $this->matlabResults = $matlabResults;
    }
    
    /**
     * @return mixed
     */
    public function getResults()
    {
        return $this->results;
    }

    /**
     * @param mixed $results
     */
    public function setResults($results)
    {
        $this->results = $results;
    }

    /**
     * @param $result
     * @return $this
     */
    public function addResult($result)
    {
        $this->results->add($result);
        
        return $this;
    }
    
    /**
     * @param $result
     * @return $this
     */
    public function addMatlabResult($result)
    {
        $this->matlabResults->add($result);
        
        return $this;
    }
    
    /**
     * @param mixed $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    /**
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }
    
    /**
     * @return mixed
     */
    public function getFish()
    {
        return $this->fish;
    }

    /**
     * @param mixed $fish
     */
    public function setFish($fish)
    {
        $this->fish = $fish;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Project
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Environment constructor.
     */
    public function __construct()
    {
        $this->privacy = Environment::ALLOW_ONLY_OWNER;
        $this->results = new ArrayCollection();
        $this->matlabResults = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getPrivacy()
    {
        return $this->privacy;
    }

    /**
     * @param mixed $privacy
     */
    public function setPrivacy($privacy)
    {
        $this->privacy = $privacy;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }
}

