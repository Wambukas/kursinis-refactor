<?php

namespace AppBundle\Entity\Fish;

use Doctrine\ORM\Mapping as ORM;

/**
 * DeathRate
 *
 * @ORM\Table(name="fish_death_rate")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Fish\DeathRateRepository")
 */
class DeathRate
{
    /**
     * @var int
     *
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="percent", type="integer")
     */
    private $percent;

    /**
     * @var int
     *
     * @ORM\Column(name="error", type="integer")
     */
    private $error;

    /**
     * @ORM\OneToOne(targetEntity="Group", inversedBy="deathRate")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    private $group;

    /**
     * @return mixed
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param mixed $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set percent
     *
     * @param integer $percent
     *
     * @return DeathRate
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * Get percent
     *
     * @return int
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * Set error
     *
     * @param integer $error
     *
     * @return DeathRate
     */
    public function setError($error)
    {
        $this->error = $error;

        return $this;
    }

    /**
     * Get error
     *
     * @return int
     */
    public function getError()
    {
        return $this->error;
    }
}

