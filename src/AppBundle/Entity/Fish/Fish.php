<?php

namespace AppBundle\Entity\Fish;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Project
 *
 * @ORM\Table(name="fish")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FishRepository")
 */
class Fish
{
    /**
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $latinName;

    /**
     * @ORM\OneToOne(targetEntity="Options", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="options_id", referencedColumnName="id")
     */
    private $defaultOptions;

    /**
     * @ORM\Column(type="string", length=255)
     * 
     * @Assert\File()
     */
    private $image;

    public function setImage($file = null)
    {
        $this->image = $file;
    }

    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getLatinName()
    {
        return $this->latinName;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param mixed $latinName
     */
    public function setLatinName($latinName)
    {
        $this->latinName = $latinName;
    }

    /**
     * @return mixed
     */
    public function getDefaultOptions()
    {
        return $this->defaultOptions;
    }

    /**
     * @param mixed $defaultOptions
     */
    public function setDefaultOptions($defaultOptions)
    {
        $this->defaultOptions = $defaultOptions;
    }

    public function __toString()
    {
        return $this->getName() . ' ('.$this->getLatinName().')';
    }
}