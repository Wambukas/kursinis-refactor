<?php

namespace AppBundle\Entity\Fish;

use Doctrine\ORM\Mapping as ORM;

/**
 * Group
 *
 * @ORM\Table(name="fish_group")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Fish\GroupRepository")
 */
class Group
{
    /**
     * @var int
     *
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(name="year", type="integer");
     */
    private $year;

    /**
     * @ORM\OneToOne(targetEntity="DeathRate", mappedBy="group", cascade={"persist", "remove"})
     */
    private $deathRate;
    
    /**
     * @ORM\OneToOne(targetEntity="Maturity", mappedBy="group", cascade={"persist", "remove"})
     */
    private $maturity;
    
    /**
     * @ORM\OneToOne(targetEntity="Reproductiveness", mappedBy="group", cascade={"persist", "remove"})
     */
    private $reproductiveness;
    
    /**
     * @ORM\OneToOne(targetEntity="Weight", mappedBy="group", cascade={"persist", "remove"})
     */
    private $weight;

    /**
     * @ORM\ManyToOne(targetEntity="Options", inversedBy="groups")
     * @ORM\JoinColumn(name="options_id", referencedColumnName="id")
     */
    private $options;

    /**
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param mixed $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }
    
    /**
     * @return mixed
     */
    public function getDeathRate()
    {
        return $this->deathRate;
    }

    /**
     * @return mixed
     */
    public function getMaturity()
    {
        return $this->maturity;
    }

    /**
     * @return mixed
     */
    public function getReproductiveness()
    {
        return $this->reproductiveness;
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $deathRate
     */
    public function setDeathRate($deathRate)
    {
        $this->deathRate = $deathRate;
    }

    /**
     * @param mixed $maturity
     */
    public function setMaturity($maturity)
    {
        $this->maturity = $maturity;
    }

    /**
     * @param mixed $reproductiveness
     */
    public function setReproductiveness($reproductiveness)
    {
        $this->reproductiveness = $reproductiveness;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }
}

