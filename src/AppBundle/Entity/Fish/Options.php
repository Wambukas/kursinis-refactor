<?php

namespace AppBundle\Entity\Fish;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Options
 *
 * @ORM\Table(name="fish_options")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Fish\OptionsRepository")
 */
class Options
{
    /**
     * @var int
     *
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Group", mappedBy="options", cascade={"persist", "remove"})
     */
    private $groups;
    
    /**
     * @return mixed
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param mixed $groups
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function addGroup($group)
    {
        $this->groups->add($group);
        
        return $this;
    }
    
    public function __construct()
    {
        $this->groups = new ArrayCollection();
    }
}

