<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Environment;
use AppBundle\Entity\Fish\Group;
use AppBundle\Entity\Fish\Options;
use AppBundle\Entity\MatlabResult;
use AppBundle\Entity\Result;
use Symfony\Component\DependencyInjection\Container;

class MathManager
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var Result
     */
    private $lastResult;

    /**
     * @var array
     */
    private $newResult;

    /**
     * @var array
     */
    private $parameterShiftResults;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->newResult = [];
    }

    public function predictMatlab(Environment $workspace)
    {        
        /** @var MatlabResult[] $results */
        $results = $workspace->getMatlabResults();

        $this->lastResult = $results[0];
        foreach ($results as $result) {
            if ($this->lastResult->getYear() < $result->getYear()) {
                $this->lastResult = $result;
            }
        }
        
        exec('Rscript R2.txt '.$this->lastResult->getResult(), $output);

        $value = floatval(substr($output[0], 4));
        
        $toSave = new MatlabResult();

        $toSave->setYear($this->lastResult->getYear()+1);
        $toSave->setResult(intval(ceil(floatval($value))));
        $toSave->setEnvironment($workspace);
        $workspace->addMatlabResult($toSave);

        return $toSave;

    }
    
    public function predict(Environment $workspace)
    {

        /** @var Result[] $results */
        $results = $workspace->getResults();

        $this->lastResult = $results[0];
        foreach ($results as $result) {
            if ($this->lastResult->getYear() < $result->getYear()) {
                $this->lastResult = $result;
            }
        }

        $newborns = $this->getNewborns($workspace);
        $this->moveToNowResult($workspace, $newborns);

        $toSave = new Result();

        $toSave->setYear($this->lastResult->getYear()+1);
        $toSave->setResult(json_encode($this->newResult));
        $toSave->setEnvironment($workspace);
        $workspace->addResult($toSave);
        
        return $toSave;
    }

    public function predictParameters(Environment $workspace)
    {
        /** @var Result[] $results */
        $results = $workspace->getResults();

        if (count($results) < 2) {
            return null;
        }

        foreach ($results as $key => $result) {
            if ($key === 0) {
                $this->initParamShift(json_decode($results[$key]->getResult(), true));
                continue;
            }
            $this->compareResults($results[$key-1], $results[$key]);
        }

        $deathRates = $this->makeDeathRateShift();

        foreach ($results as $key => $result) {
            if ($key === 0) {
                $this->initParamShift(json_decode($results[$key]->getResult(), true));
                continue;
            }
            $this->compareResultsBorn($results[$key-1], $results[$key]);
        }
        $reproductiveness = $this->makeReproductivenessShift();
        $maturity = $this->makeMaturityShift(count(json_decode($results[0]->getResult(), true)));
        
        return [
            'deathRate' => $deathRates,
            'maturity' => $maturity,
            'reproductiveness' => $reproductiveness,
        ];
    }

    private function makeDeathRateShift()
    {
        foreach ($this->parameterShiftResults as $key => $shift) {
            if (count($shift) > 0) {
                $result = 1 - array_sum($shift)/count($shift);
                if ($result >= 1) {
                    $this->parameterShiftResults[$key] = 100;
                } else {
                    $this->parameterShiftResults[$key] = (int)round($result*100);
                }
            } else {
                $this->parameterShiftResults[$key] = 0;
            }
        }

        return $this->parameterShiftResults;
    }

    private function makeMaturityShift($count)
    {
        $maturity = [];
        for ($i = 0; $i < $count; $i++) {
            if ($i< 3) {
                $maturity[] = 0;
            } else {
                $maturity[] = 100;
            }
        }
        
        return $maturity;
    }
    
    private function makeReproductivenessShift()
    {
        foreach ($this->parameterShiftResults as $key => $shift) {
            if (count($shift) > 0) {
                $this->parameterShiftResults[$key] = array_sum($shift)/count($shift);
            } else {
                $this->parameterShiftResults[$key] = 0.00;
            }
        }

        return $this->parameterShiftResults;
    }

    private function compareResults(Result $olderGroup, Result $newerGroup)
    {
        $olderResults = json_decode($olderGroup->getResult(), true);
        $newerResults = json_decode($newerGroup->getResult(), true);
        for ($i = 0; $i < count($olderResults); $i++) {
            if ($i === 0) {
                continue;
            }

            $this->compareGroups($i, $olderResults[$i-1], $newerResults[$i]);
        }
    }

    private function compareResultsBorn(Result $olderGroup, Result $newerGroup)
    {
        $olderResults = json_decode($olderGroup->getResult(), true);
        $newerResults = json_decode($newerGroup->getResult(), true);
        $newborns = $newerResults[0];

        $total = 0;
        for ($i = 0; $i < count($olderResults); $i++) {
            if ($i < 3) {
                continue;
            }
            $total += $olderResults[$i];
        }
        $temp = $newborns/$total;
        $single = ($newborns/$total)/(count($olderResults)-3);
        $offset = (int)(count($olderResults)-3)/2;
        $temp = $temp - $offset*$single;
        for ($i = 0; $i < count($olderResults)-3; $i++) {
            $this->parameterShiftResults[$i+3][] = $temp;
            $temp = $temp+$single;
        }
    }

    private function compareGroups($i, $older, $newer)
    {
        if ($older != 0) {
            $this->parameterShiftResults[$i][] = $newer/$older;
        }
    }

    private function initParamShift($groups)
    {
        foreach ($groups as $key => $group)
        {
            $this->parameterShiftResults[$key] = [];
        }
    }

    private function moveToNowResult(Environment $workspace, $newborns)
    {
        $options = $workspace->getOptions();

        $this->newResult[0] =
            $newborns;
        foreach (json_decode($this->lastResult->getResult(), true) as $key => $result) {
            $this->newResult[$key+1] =
                $result*
                (
                    100-$options->getGroups()[$key]->getDeathRate()->getPercent()+
                    mt_rand(-$options->getGroups()[$key]->getDeathRate()->getError(), $options->getGroups()[$key]->getDeathRate()->getError())
                )/
                100;
        }

        $this->newResult[count($this->newResult)-2] += $this->newResult[count($this->newResult)-1];

        unset($this->newResult[count($this->newResult)-1]);
    }

    private function getNewborns(Environment $workspace)
    {
        /** @var Options $options */
        $options = $workspace->getOptions();
        
        $values = json_decode($this->lastResult->getResult(), true);

        $newborns = 0;
        foreach ($values as $key => $value) {
            $newborns +=
                (
                    $value*
                    $options->getGroups()[$key]->getMaturity()->getPercent()/100
                )*
                $options->getGroups()[$key]->getReproductiveness()->getAmount()*
                (
                    100-$options->getGroups()[$key]->getDeathRate()->getPercent()+
                    mt_rand(-$options->getGroups()[$key]->getDeathRate()->getError(), $options->getGroups()[$key]->getDeathRate()->getError())
                )/
                100
            /2;
        }

        return $newborns;
    }

    protected function up($times)
    {
        $value = 1;

        for($i = 0; $i < (int)$times; $i++) {
            $value = $value*10;
        }

        return $value;
    }
}