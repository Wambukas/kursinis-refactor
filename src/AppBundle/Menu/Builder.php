<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $menu->addChild('Pagrindinis', array('route' => 'homepage'));

        $menu->addChild('Vartotojai', array('route' => 'users'));

        $menu->addChild('Aplinkos', array('route' => 'environments'));

        $menu->addChild('Žuvys', array('route' => 'fish'));

        $menu->addChild('Projektai', array('route' => 'projects'));

        return $menu;
    }
}