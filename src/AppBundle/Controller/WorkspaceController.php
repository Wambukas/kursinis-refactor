<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Environment;
use AppBundle\Entity\Fish\DeathRate;
use AppBundle\Entity\Fish\Group;
use AppBundle\Entity\Fish\Maturity;
use AppBundle\Entity\Fish\Options;
use AppBundle\Entity\Fish\Reproductiveness;
use AppBundle\Entity\Fish\Weight;
use AppBundle\Entity\MatlabResult;
use AppBundle\Entity\Result;
use AppBundle\Manager\MathManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/workspace")
 */
class WorkspaceController extends Controller
{
    /**
     * @var Environment
     */
    private $environment;

    /**
     * @var MathManager
     */
    private $mathManager;

    /**
     * @Route("/", name="workspace")
     */
    public function indexAction(Request $request)
    {
        $return = $this->getWorkspace($request);

        if ($return instanceof RedirectResponse) {
            return $return;
        }

        $maxX = 0;
        $minX = null;
        $maxY = 0;
        $minY = 0;

        /**
         * @var integer $key
         * @var Result $result
         */
        foreach ($this->environment->getResults() as $key => $result) {
            $data = json_decode($result->getResult(), true);
            $year = $result->getYear();
            if ($year < $minX || $minX === null) {
                $minX = $year;
            } 
            
            if ($year > $maxX) {
                $maxX = $year;
            }
            
            foreach ($data as $value) {
                if ($value > $maxY) {
                    $maxY = $value;
                }
            }
        }

        $maxX += 1;
        $minX += -1;

        $mmaxX = null;
        $mmaxY = null;
        $mminX = null;
        $mminY = null;
        
        foreach ($this->environment->getMatlabResults() as $key => $result) {
            if ($mmaxX === null) {
                $mmaxX = $result->getYear();
            }
            if ($mminX === null) {
                $mminX = $result->getYear();
            }
            if ($mmaxY === null) {
                $mmaxY = $result->getResult();
            }
            if ($mminY === null) {
                $mminY = $result->getResult();
            }
            
            if ($mmaxX  < $result->getYear()) {
                $mmaxX = $result->getYear();
            }
            if ($mminX > $result->getYear()) {
                $mminX = $result->getYear();
            }
            
            if ($mmaxY < $result->getResult()) {
                $mmaxY = $result->getResult();
            }
            
            if ($mminY > $result->getResult()) {
                $mminY = $result->getResult();
            }
        }
        
        return $this->render('AppBundle:Workspace:index.html.twig', [
            'environment' => $this->environment,
            'measures' => ['maxX' => $maxX, 'maxY' => $maxY, 'minX' => $minX, 'minY' => $minY,
                'mmaxX' => $mmaxX+1, 'mmaxY' => $mmaxY, 'mminX' => $mminX-1, 'mminY' => $mminY],
        ]); 
    }

    /**
     * @Route("/select/{environmentId}", name="select_workspace")
     */
    public function selectAction(Request $request, $environmentId)
    {
        $response = $this->redirectToRoute('workspace');

        $response->headers->setCookie(new Cookie('workspace', $environmentId));

        return $response;
    }

    /**
     * @Route("/create", name="create_result")
     * @Method("POST")
     */
    public function createResultAction(Request $request)
    {
        $return = $this->getWorkspace($request);

        if ($return instanceof RedirectResponse) {
            return $return;
        }
        
        $groups = $request->request->get('groups');
        $year = $request->request->get('year');
        
        $result = new Result();
        
        $result->setYear($year);
        
        $result->setResult(json_encode($groups));
        
        $result->setEnvironment($this->environment);
        
        $this->getDoctrine()->getManager()->persist($result);
        $this->getDoctrine()->getManager()->flush();
        
        return $this->redirectToRoute('workspace');
    }

    /**
     * @Route("/create-matlab", name="create_matlab_result")
     * @Method("POST")
     */
    public function createMatlabResultAction(Request $request)
    {
        $return = $this->getWorkspace($request);

        if ($return instanceof RedirectResponse) {
            return $return;
        }

        $resultInteger = $request->request->get('result');
        $year = $request->request->get('year');

        $result = new MatlabResult();

        $result->setYear($year);

        $result->setResult($resultInteger);

        $result->setEnvironment($this->environment);

        $this->getDoctrine()->getManager()->persist($result);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('workspace');
    }

    /**
     * @Route("/edit-parameters", name="edit_parameters")
     * @Method("POST")
     */
    public function editParametersAction(Request $request)
    {

        /** @var Environment $environment */
        $environment = $this->getDoctrine()->getRepository(Environment::class)->find($request->request->get('environment'));
        $groupsArray = $request->request->get('groups');

        $options = new Options();

        if ($groupsArray) {
            foreach ($groupsArray as $groupArray) {

                $group = new Group();
                $group->setYear($groupArray['year']);

                $deathRate = new DeathRate();
                $deathRate->setError($groupArray['deathRate']['error']);
                $deathRate->setPercent($groupArray['deathRate']['percent']);

                $deathRate->setGroup($group);
                $group->setDeathRate($deathRate);

                $maturity = new Maturity();
                $maturity->setPercent($groupArray['maturity']['percent']);

                $maturity->setGroup($group);
                $group->setMaturity($maturity);

                $reproductiveness = new Reproductiveness();
                $reproductiveness->setAmount($groupArray['reproductiveness']['amount']);

                $reproductiveness->setGroup($group);
                $group->setReproductiveness($reproductiveness);

                $weight = new Weight();
                $weight->setAmount($groupArray['weight']['amount']);

                $weight->setGroup($group);
                $group->setWeight($weight);

                $group->setOptions($options);
                $options->addGroup($group);
            }
        }

        $environment->setOptions($options);

        $this->getDoctrine()->getManager()->persist($environment);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('workspace');
    }

    /**
     * @Route("/predict", name="predict_result")
     * @Method("GET")
     */
    public function predictAction(Request $request)
    {
        $return = $this->getWorkspace($request);

        if ($return instanceof RedirectResponse) {
            return $return;
        }
        
        $newResult = $this->mathManager->predict($this->environment);

        $this->getDoctrine()->getManager()->persist($newResult);
        $this->getDoctrine()->getManager()->flush();
        
        return $this->redirectToRoute('workspace');
    }

    /**
     * @Route("/predict-matlab", name="predict_result_matlab")
     * @Method("GET")
     */
    public function predictMatlabAction(Request $request)
    {
        $return = $this->getWorkspace($request);

        if ($return instanceof RedirectResponse) {
            return $return;
        }

        $newResult = $this->mathManager->predictMatlab($this->environment);

        $this->getDoctrine()->getManager()->persist($newResult);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('workspace');
    }

    /**
     * @Route("/predict-parameters", name="predict_parameters")
     * @Method("GET")
     */
    public function predictParameters(Request $request)
    {
        $return = $this->getWorkspace($request);

        if ($return instanceof RedirectResponse) {
            return $return;
        }

        $params = $this->mathManager->predictParameters($this->environment);

        $options = new Options();

        foreach ($params['maturity'] as $key => $param) {
            $group = new Group();
            $group->setYear($key);

            $deathRate = new DeathRate();
            $deathRate->setError(0);
            $deathRate->setPercent($params['deathRate'][$key]);

            $deathRate->setGroup($group);
            $group->setDeathRate($deathRate);

            $maturity = new Maturity();
            $maturity->setPercent($params['maturity'][$key]);

            $maturity->setGroup($group);
            $group->setMaturity($maturity);

            $reproductiveness = new Reproductiveness();
            $reproductiveness->setAmount($params['reproductiveness'][$key]);

            $reproductiveness->setGroup($group);
            $group->setReproductiveness($reproductiveness);

            $weight = new Weight();
            $weight->setAmount(0);

            $weight->setGroup($group);
            $group->setWeight($weight);

            $group->setOptions($options);
            $options->addGroup($group);
        }

        $this->environment->setOptions($options);

        $this->getDoctrine()->getManager()->persist($this->environment);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('workspace');
    }
    
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function getWorkspace(Request $request)
    {
        $this->mathManager = $this->container->get('fish.math');
        
        $id = $request->cookies->get('workspace');
        
        if ($id === NULL) {
            return $this->redirectToRoute('environments');
        }

        $this->environment = $this->getDoctrine()->getRepository(Environment::class)->find($id);

        if (!$this->environment) {
            return $this->redirectToRoute('environments');
        }
    }
}