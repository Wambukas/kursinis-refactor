<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Environment;
use AppBundle\Entity\Fish\DeathRate;
use AppBundle\Entity\Fish\Fish;
use AppBundle\Entity\Fish\Group;
use AppBundle\Entity\Fish\Maturity;
use AppBundle\Entity\Fish\Options;
use AppBundle\Entity\Fish\Reproductiveness;
use AppBundle\Entity\Fish\Weight;
use AppBundle\Entity\Project;
use AppBundle\Entity\User;
use AppBundle\Form\EnvironmentType;
use AppBundle\Form\Fish\FishEditType;
use AppBundle\Form\Fish\FishType;
use AppBundle\Form\ProjectType;
use AppBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Tests\Encoder\PasswordEncoder;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class DefaultController extends Controller
{
    /**
     * @Route("/login", name="login")
     * @Method({"GET", "POST"})
     */
    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();

        $lastUsername = $authenticationUtils->getLastUsername();

        if ($request->getMethod() === 'POST') {
            $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $request->request->get('_username')]);
            $plainPassword = $request->request->get('_password');
            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($user, $plainPassword);
            $token = new UsernamePasswordToken($user, $encoded, 'main', $user->getRoles());
            $this->get("security.token_storage")->setToken($token);

            return $this->redirectToRoute('homepage');
        }

        return $this->render('default/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction(Request $request)
    {
        $this->get("security.token_storage")->setToken(null);
        
        return $this->redirectToRoute('login');
    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('default/index.html.twig');
    }
    
    /**
     * @Route("/users", name="users")
     */
    public function usersAction(Request $request)
    {
        if ($this->getUser()->getRole() === User::ROLE_MASTER) {
            return $this->usersActionForMaster($request);
        }
        $user = $this->getUser();

        if ($request->query->get('verification')) {
            $verification = $this->generateUrl('verify_user', ['hash' => $request->query->get('verification')], UrlGeneratorInterface::ABSOLUTE_URL);
        } else {
            $verification = null;
        }

        return $this->render('AppBundle::users.html.twig', [ 'user' => $user, 'verification' => $verification ]);
    }
    
    protected function usersActionForMaster(Request $request)
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();


        if ($request->query->get('verification')) {
            $verification = $this->generateUrl('verify_user', ['hash' => $request->query->get('verification')], UrlGeneratorInterface::ABSOLUTE_URL);
        } else {
            $verification = null;
        }

        return $this->render('AppBundle::users.html.twig', [ 'users' => $users, 'verification' => $verification  ]);
    }

    /**
     * @Route("/add-user/{projectId}", name="add-user")
     */
    public function addUser(Request $request, $projectId)
    {
        if ($this->getUser()->getRole() === User::ROLE_MASTER) {
            $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        } else {
            $users = [$this->getUser()->getProject()->getUsers()];
        }

        /** @var Project $user */
        $project = $this->getDoctrine()->getRepository(Project::class)->find($projectId);

        if ($request->isMethod('POST')) {
            /** @var User $project */
            $user = $this->getDoctrine()->getRepository(User::class)->find($request->request->get('userId'));

            $user->setProject($project);

            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();
            
            return $this->redirectToRoute('users');
        }

        return $this->render('AppBundle::add-user.html.twig', array(
            'users' => $users,
            'project' => $project,
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/create-user", name="create-user")
     */
    public function createUserAction(Request $request)
    {
        $user = new User();

        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $project = $this->getUser()->getProject();

            if ($project) {
                $user->setProject($project);
            }
            
            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($user, 'really_secret');

            $user->setPassword($encoded);
            
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('users', ['verification' => $user->getHash()]);
        }

        return $this->render('AppBundle::create-user.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/verify-user/{hash}", name="verify_user")
     * @Method({"GET", "POST"})
     */
    public function verifyUserAction(Request $request, $hash)
    {
        /** @var User $user */
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['hash' => $hash]);
        $errors = ['password' => false];

        if (!$user) {
            return $this->redirectToRoute('login');
        }

        if ($request->getMethod() === 'POST') {
            if ($request->request->get('_password') === $request->request->get('_password_repeat')) {

                $plainPassword = $request->request->get('_password');
                $encoder = $this->container->get('security.password_encoder');
                $encoded = $encoder->encodePassword($user, $plainPassword);
                
                $user->setPassword($encoded);
                $user->setIsActive(true);

                $this->getDoctrine()->getManager()->persist($user);
                $this->getDoctrine()->getManager()->flush();

                $token = new UsernamePasswordToken($user, $encoded, 'main', $user->getRoles());
                $this->get("security.token_storage")->setToken($token);

                return $this->redirectToRoute('homepage');
                
            } else {
                $errors['password'] = true;
            }
        }
        
        return $this->render('default/verify.html.twig', ['user' => $user, 'errors' => $errors]);
    }

    /**
     * @Route("/create-environment", name="create-environment")
     */
    public function createEnvironmentAction(Request $request)
    {
        $environment = new Environment();

        $form = $this->createForm(EnvironmentType::class, $environment);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            $environment->setOwner($this->getUser());

            /** @var Options $options */
            $options = $environment->getFish()->getDefaultOptions();

            $environment->setOptions($this->cloneOptions($options));
            $this->getDoctrine()->getManager()->persist($environment);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('environments');
        }
        
        return $this->render('AppBundle::create-environment.html.twig', array(
            'form' => $form->createView(),
        ));
    }
    
    /**
     * @Route("/environments", name="environments")
     */
    public function environmentsAction()
    {
        if ($this->getUser()->getRole() === User::ROLE_MASTER) {

            return $this->environmentsActionForMaster();
        }
        
        $environments = $this->getUser()->getEnvironments();
        
        return $this->render('AppBundle::environments.html.twig', [ 'environments' => $environments ]);
    }
    
    protected function environmentsActionForMaster()
    {
        $environments = $this->getDoctrine()->getRepository(Environment::class)->findAll();

        return $this->render('AppBundle::environments.html.twig', [ 'environments' => $environments ]);
    }  
    
    /**
     * @Route("/projects", name="projects")
     */
    public function projectsAction()
    {
        if ($this->getUser()->getRole() !== User::ROLE_MASTER) {
            return $this->redirectToRoute('homepage');
        }

        $projects = $this->getDoctrine()->getRepository(Project::class)->findAll();
        
        return $this->render('AppBundle::projects.html.twig', [ 'projects' => $projects ]);
    }

    /**
     * @Route("/create-projects", name="create-project")
     */
    public function createProjectAction(Request $request)
    {
        $project = new Project();

        $form = $this->createForm(ProjectType::class, $project);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $this->getDoctrine()->getManager()->persist($project);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('projects');
        }

        return $this->render('AppBundle::create-project.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/fish", name="fish")
     */
    public function fishAction()
    {
        $fish = $this->getDoctrine()->getRepository(Fish::class)->findAll();
        
        return $this->render('AppBundle:Fish:fish.html.twig', [ 'fish' => $fish ]);
    }

    /**
     * @Route("/edit-fish/{id}", name="edit-fish")
     */
    public function editFishAction(Request $request, $id)
    {
        $fish = $this->getDoctrine()->getRepository(Fish::class)->find($id);

        if (!$fish) {
            return $this->redirectToRoute('fish');
        }

        $form = $this->createForm(FishEditType::class, $fish);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $this->getDoctrine()->getManager()->persist($fish);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('fish');
        }

        return $this->render('AppBundle:Fish:edit-fish.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/delete-fish/{id}", name="delete-fish")
     */
    public function deleteFishAction($id)
    {
        /** @var Fish $fish */
        $fish = $this->getDoctrine()->getRepository(Fish::class)->find($id);
        
        $this->getDoctrine()->getManager()->remove($fish);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('fish');
    }

    /**
     * @Route("/create-fish", name="create-fish")
     */
    public function createFishAction(Request $request)
    {
        $fish = new Fish();
        
        $form = $this->createForm(FishType::class, $fish);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $file = $fish->getImage();

            if ($file) {
                $fileName = md5(uniqid()).'.jpg';
                $file->move(
                    $this->getParameter('fish_directory'),
                    $fileName
                );

                $fish->setImage($fileName);
            }

            $groupsArray = $request->request->get('groups');
            $options = new Options();
            
            if ($groupsArray) {
                foreach ($groupsArray as $groupArray) {

                    $group = new Group();
                    $group->setYear($groupArray['year']);

                    $deathRate = new DeathRate();
                    $deathRate->setError($groupArray['deathRate']['error']);
                    $deathRate->setPercent($groupArray['deathRate']['percent']);

                    $deathRate->setGroup($group);
                    $group->setDeathRate($deathRate);

                    $maturity = new Maturity();
                    $maturity->setPercent($groupArray['maturity']['percent']);

                    $maturity->setGroup($group);
                    $group->setMaturity($maturity);

                    $reproductiveness = new Reproductiveness();
                    $reproductiveness->setAmount($groupArray['reproductiveness']['amount']);

                    $reproductiveness->setGroup($group);
                    $group->setReproductiveness($reproductiveness);

                    $weight = new Weight();
                    $weight->setAmount($groupArray['weight']['amount']);

                    $weight->setGroup($group);
                    $group->setWeight($weight);

                    $group->setOptions($options);
                    $options->addGroup($group);
                }
            }

            $fish->setDefaultOptions($options);

            $this->getDoctrine()->getManager()->persist($fish);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('fish');
        }

        return $this->render('AppBundle:Fish:create-fish.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @param Options $options
     *
     * @return Options
     */
    private function cloneOptions(Options $options)
    {
        $newOptions = new Options();
        /** @var Group $group */
        foreach ($options->getGroups() as $group) {
            $newGroup = new Group();

            $newGroup->setYear($group->getYear());
            
            $deathRate = $group->getDeathRate();

            $newDeathRate = new DeathRate();
            $newDeathRate->setPercent($deathRate->getPercent());
            $newDeathRate->setError($deathRate->getError());

            $newGroup->setDeathRate($newDeathRate);
            $newDeathRate->setGroup($newGroup);

            $maturity = $group->getMaturity();

            $newMaturity = new Maturity();
            $newMaturity->setPercent($maturity->getPercent());

            $newGroup->setMaturity($newMaturity);
            $newMaturity->setGroup($newGroup);

            $reproductiveness = $group->getReproductiveness();

            $newReproductiveness = new Reproductiveness();
            $newReproductiveness->setAmount($reproductiveness->getAmount());

            $newGroup->setReproductiveness($newReproductiveness);
            $newReproductiveness->setGroup($newGroup);

            $weight = $group->getWeight();

            $newWeight = new Weight();
            $newWeight->setAmount($weight->getAmount());

            $newGroup->setWeight($newWeight);
            $newWeight->setGroup($newGroup);

            $newGroup->setOptions($newOptions);
            $newOptions->addGroup($newGroup);
        }

        $this->getDoctrine()->getManager()->persist($newOptions);
        $this->getDoctrine()->getManager()->flush();

        return $newOptions;
    }
}
