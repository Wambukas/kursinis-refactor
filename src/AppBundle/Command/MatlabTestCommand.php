<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MatlabTestCommand extends ContainerAwareCommand
{
    const MATLAB_CODE = "rng shuffle; N = 20000; gama = 0.7; alfa = rand(1); if(alfa > 0.8); if (alfa > 0.95); alfa = 0.8; else; alfa = 0.6; end; else; alfa = 1; end; beta = rand(1); if(beta > 0.3); if (beta > 0.7); beta = 1.2; else; beta = 1; end; else; beta = 0.8; end; a = normrnd(25000,15000)*0.5*N*alfa*gama; while a < 0; a = normrnd(25000,15000)*0.5*N*alfa*gama; end; b = normrnd(a,a/4); while b < 0; b = normrnd(a,a/4); end; WHAT = (0.8*beta*a*N*alfa*gama)/(b+0.5*N*alfa*gama); if (WHAT < 0); WHAT = 0; end; NP = N+WHAT-0.2*N-(1-alfa)*N - (1-gama)*N; disp(NP); quit();";

    protected function configure()
    {
        $this->setName('app:test:r');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        exec('Rscript R2.txt 15000', $output);

        $value = substr($output[0], 4);

        var_dump(intval(ceil(floatval($value))));
    }

    protected function executea(InputInterface $input, OutputInterface $output)
    {
        $command = "matlab -nojvm -nodesktop -wait -nodisplay -logfile out.txt -r \"".self::MATLAB_CODE."\"";
        exec($command, $outputMatlab);

        $value = null;

        $name = bin2hex(mcrypt_create_iv(22, MCRYPT_DEV_URANDOM));
        
        $handle = fopen($name.".txt", "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                if (strpos($line, 'e+') !== false) {
                    $value = trim($line);

                    $data = explode('e+', $value);

                    $value = $data[0]*$this->up($data[1]);
                }
            }

            fclose($handle);
        }

        echo $value;

        unlink($name.'.txt');
    }

    protected function up($times)
    {
        $value = 1;

        for($i = 0; $i < (int)$times; $i++) {
            $value = $value*10;
        }

        return $value;
    }
}