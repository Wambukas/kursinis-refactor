<?php

namespace AppBundle\Command;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FixturesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:fixtures');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->createMasters();
    }

    protected function createMasters()
    {
        $master = new User();
        $master->setName('admin');
        $master->setSurname('admin');
        $master->setEmail('admin@vu.drebule.com');
        $master->setRole(User::ROLE_MASTER);
        $master->setIsActive(true);
        $encoder = $this->getContainer()->get('security.password_encoder');
        $encoded = $encoder->encodePassword($master, 'labutis');
        $master->setPassword($encoded);

        $this->getContainer()->get('doctrine.orm.entity_manager')->persist($master);
        $this->getContainer()->get('doctrine.orm.entity_manager')->flush();
    }

}